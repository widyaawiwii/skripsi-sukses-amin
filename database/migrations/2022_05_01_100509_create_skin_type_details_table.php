<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkinTypeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skin_type_details', function (Blueprint $table) {
            $table->string('skin_type_id')->unique();
            $table->string('skin_type_name');
            $table->text('skin_type_desc_p1');
            $table->text('skin_type_desc_p2');
            $table->text('skin_type_desc_p3');
            $table->string('skin_type_desc_img');
            $table->string('skin_type_face_img');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skin_type_details');
    }
}
