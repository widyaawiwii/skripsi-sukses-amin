<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_answers', function (Blueprint $table) {
            $table->id();
            $table->string('questions1')->nullable();
            $table->string('questions2')->nullable();
            $table->string('questions3')->nullable();
            $table->string('questions4')->nullable();
            $table->string('questions5')->nullable();
            $table->string('questions6')->nullable();
            $table->string('questions7')->nullable();
            $table->string('questions8')->nullable();
            $table->string('questions9')->nullable();
            $table->string('questions10')->nullable();
            $table->string('questions11')->nullable();
            $table->string('questions12')->nullable();
            $table->string('questions13')->nullable();
            $table->string('questions14')->nullable();
            $table->string('questions15')->nullable();
            $table->string('questions16')->nullable();
            $table->string('questions17')->nullable();
            $table->string('questions18')->nullable();
            $table->string('questions19')->nullable();
            $table->string('questions20')->nullable();
            $table->string('questions21')->nullable();
            $table->string('questions22')->nullable();
            $table->float('value22')->nullable()->default(null);
            $table->string('questions23')->nullable();
            $table->float('value23')->nullable()->default(null);
            $table->string('questions24')->nullable();
            $table->float('value24')->nullable()->default(null);
            $table->string('questions25')->nullable();
            $table->float('value25')->nullable()->default(null);
            $table->string('questions26')->nullable();
            $table->string('questions27')->nullable();
            $table->string('questions28')->nullable();
            $table->string('questions29')->nullable();
            $table->string('questions30')->nullable();
            $table->string('questions31')->nullable();
            $table->string('questions32')->nullable();
            $table->string('questions33')->nullable();
            $table->string('questions34')->nullable();
            $table->string('questions35')->nullable();
            $table->string('questions36')->nullable();
            $table->string('questions37')->nullable();
            $table->string('questions38')->nullable();
            $table->string('questions39')->nullable();
            $table->string('questions40')->nullable();
            $table->string('questions41')->nullable();
            $table->string('result');
            $table->boolean('status')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_answers');
    }
}
