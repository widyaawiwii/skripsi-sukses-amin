<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMappingBaumannTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_baumann', function (Blueprint $table) {
            $table->integer('id_question');
            $table->integer('baumann_no');
            $table->string('quiz_answer');
            $table->string('baumann_answer');
            $table->float('value')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapping_baumann');
    }
}
