<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MappingBaumannTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mapping_baumann')->insert([
            [
                'id_question'=> '1',
                'baumann_no'=> '1',
                'quiz_answer'=> 'Kencang',
                'baumann_answer' => 'Ketat',
                'value' => null
            ],
            [
                'id_question'=> '1',
                'baumann_no'=> '1',
                'quiz_answer'=> 'Terhidrasi dengan baik dan tidak terlihat berminyak',
                'baumann_answer' => 'Terhidrasi dengan baik tanpa kilapan',
                'value' => null
            ],
            [
                'id_question'=> '1',
                'baumann_no'=> '1',
                'quiz_answer'=> 'Terhidrasi dengan baik dan terlihat berminyak',
                'baumann_answer' => 'Mengkilap',
                'value' => null
            ],

            [
                'id_question'=> '2',
                'baumann_no'=> '2',
                'quiz_answer'=> 'Tidak pernah',
                'baumann_answer' => 'Tidak pernah, atau Anda tidak pernah menyadari kilauan',
                'value' => null
            ],

            [
                'id_question'=> '3',
                'baumann_no'=> '3',
                'quiz_answer'=> 'Flaky, atau cakey',
                'baumann_answer' => 'Terkelupas atau berlapis kerutan',
                'value' => null
            ],
            [
                'id_question'=> '3',
                'baumann_no'=> '3',
                'quiz_answer'=> 'Terlihat berminyak',
                'baumann_answer' => 'Berkilau',
                'value' => null
            ],
            [
                'id_question'=> '3',
                'baumann_no'=> '3',
                'quiz_answer'=> 'Terlihat bergaris-garis dan berminyak',
                'baumann_answer' => 'Bergaris-garis dan mengkilap',
                'value' => null
            ],
            [
                'id_question'=> '3',
                'baumann_no'=> '3',
                'quiz_answer'=> 'Tidak menggunakan make up',
                'baumann_answer' => 'Saya tidak mengunakan foundation',
                'value' => null
            ],
//
            [
                'id_question'=> '4',
                'baumann_no'=> '4',
                'quiz_answer'=> 'Kering dan pecah-pecah',
                'baumann_answer' => 'Terasa sangat kering atau pecah-pecah',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '7',
                'quiz_answer'=> 'Kering dan pecah-pecah',
                'baumann_answer' => 'Selalu',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '8',
                'quiz_answer'=> 'Kering dan pecah-pecah',
                'baumann_answer' => 'Sangat kasar, pecah-pecah, atau pucat',
                'value' => null
            ],

            [
                'id_question'=> '4',
                'baumann_no'=> '4',
                'quiz_answer'=> 'Kencang',
                'baumann_answer' => 'Terasa kencang',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '7',
                'quiz_answer'=> 'Kencang',
                'baumann_answer' => 'Terasa kencang',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '8',
                'quiz_answer'=> 'Kencang',
                'baumann_answer' => 'Halus',
                'value' => null
            ],

            [
                'id_question'=> '4',
                'baumann_no'=> '4',
                'quiz_answer'=> 'Normal',
                'baumann_answer' => 'Terasa biasa',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '7',
                'quiz_answer'=> 'Normal',
                'baumann_answer' => 'Jarang',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '8',
                'quiz_answer'=> 'Normal',
                'baumann_answer' => 'Sedikit mengkilap',
                'value' => null
            ],

            [
                'id_question'=> '4',
                'baumann_no'=> '4',
                'quiz_answer'=> 'Terlihat berminyak, atau tidak pernah merasa membutuhkan moisturizer',
                'baumann_answer' => 'Terlihat berkilau, atau saya tidak pernah merasa membutuhkan pelembab',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '7',
                'quiz_answer'=> 'Terlihat berminyak, atau tidak pernah merasa membutuhkan moisturizer',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '8',
                'quiz_answer'=> 'Terlihat berminyak, atau tidak pernah merasa membutuhkan moisturizer',
                'baumann_answer' => 'Mengkilap dan licin, atau saya tidak menggunakan pelembab',
                'value' => null
            ],

            [
                'id_question'=> '4',
                'baumann_no'=> '4',
                'quiz_answer'=> 'Tidak tahu karena selalu memakai moisturizer/sunscreen',
                'baumann_answer' => 'Tidak tahu',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '7',
                'quiz_answer'=> 'Tidak tahu karena selalu memakai moisturizer/sunscreen',
                'baumann_answer' => '',
                'value' => null
            ],
            [
                'id_question'=> '4',
                'baumann_no'=> '8',
                'quiz_answer'=> 'Tidak tahu karena selalu memakai moisturizer/sunscreen',
                'baumann_answer' => '',
                'value' => null
            ],
// 9;10;12;13;14;15;16;21
            [
                'id_question'=> '8',
                'baumann_no'=> '9',
                'quiz_answer'=> 'Terkadang atau sering ada jerawat merah',
                'baumann_answer' => 'Minimal sebulan sekali',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '9',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '10',
                'quiz_answer'=> 'Produk skincare / makeup bisa menyebabkan jerawatan, kemerahan, gatal, kepedihan, atau bengkak',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '10',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '12',
                'quiz_answer'=> 'Perhiasan yang bukan emas bisa menyebabkan kemerahan',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '12',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '13',
                'quiz_answer'=> 'Sunscreen bisa menyebabkan kemerahan atau gatal',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '13',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '14',
                'quiz_answer'=> 'Cincin bisa menyebabkan kemerahan',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '14',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '15',
                'quiz_answer'=> 'Bubble bath / massage oil / body lotion bisa menyebabkan gatal atau kering',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '15',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '16',
                'quiz_answer'=> 'Sabun dari hotel bisa menyebabkan gatal, kemerahan, atau jerawatan',
                'baumann_answer' => 'Tidak, kulit saya gatal, memerah, atau pecah-pecah',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '16',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Ya',
                'value' => null
            ],

            [
                'id_question'=> '8',
                'baumann_no'=> '21',
                'quiz_answer'=> 'Produk skincare / makeup bisa menyebabkan jerawatan, kemerahan, gatal, kepedihan, atau bengkak',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '21',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],
            [
                'id_question'=> '8',
                'baumann_no'=> '21',
                'quiz_answer'=> 'Tidak ada yang sesuai',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],
//
            [
                'id_question'=> '9',
                'baumann_no'=> '17',
                'quiz_answer'=> 'Setelah olahraga / saat stres / beremosi kuat (seperti marah)',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '9',
                'baumann_no'=> '17',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '9',
                'baumann_no'=> '18',
                'quiz_answer'=> 'Setelah makan pedas atau mengkonsumsi panas',
                'baumann_answer' => 'Sering',
                'value' => null
            ],
            [
                'id_question'=> '9',
                'baumann_no'=> '18',
                'quiz_answer'=> 'NOOOOO',
                'baumann_answer' => 'Tidak pernah',
                'value' => null
            ],

            [
                'id_question'=> '12',
                'baumann_no'=> '22',
                'quiz_answer'=> 'Tidak pernah atau saya tidak memperhatikan',
                'baumann_answer' => 'Tidak pernah atau saya tidak memperhatikan',
                'value' => 1
            ],
            [
                'id_question'=> '12',
                'baumann_no'=> '22',
                'quiz_answer'=> 'Kadang-kadang',
                'baumann_answer' => 'Kadang-kadang',
                'value' => 2
            ],
            [
                'id_question'=> '12',
                'baumann_no'=> '22',
                'quiz_answer'=> 'Sering',
                'baumann_answer' => 'Sering',
                'value' => 3
            ],
            [
                'id_question'=> '12',
                'baumann_no'=> '22',
                'quiz_answer'=> 'Selalu',
                'baumann_answer' => 'Selalu',
                'value' => 4
            ],
            //
            [
                'id_question'=> '13',
                'baumann_no'=> '23',
                'quiz_answer'=> 'Saya tidak mendapatkan bekas luka atau saya tidak memperhatikan',
                'baumann_answer' => 'Saya tidak mendapatkan bekas luka atau saya tidak memperhatikan',
                'value' => 1
            ],
            [
                'id_question'=> '13',
                'baumann_no'=> '23',
                'quiz_answer'=> 'Seminggu',
                'baumann_answer' => 'Seminggu',
                'value' => 2
            ],
            [
                'id_question'=> '13',
                'baumann_no'=> '23',
                'quiz_answer'=> 'Beberapa minggu',
                'baumann_answer' => 'Beberapa minggu',
                'value' => 3
            ],
            [
                'id_question'=> '13',
                'baumann_no'=> '23',
                'quiz_answer'=> 'Berbulan-bulan',
                'baumann_answer' => 'Berbulan-bulan',
                'value' => 4
            ],

            [
                'id_question'=> '14',
                'baumann_no'=> '24',
                'quiz_answer'=> 'Tidak ada',
                'baumann_answer' => 'Tidak ada',
                'value' => 1
            ],
            [
                'id_question'=> '14',
                'baumann_no'=> '24',
                'quiz_answer'=> 'Satu',
                'baumann_answer' => 'Satu',
                'value' => 2
            ],
            [
                'id_question'=> '14',
                'baumann_no'=> '24',
                'quiz_answer'=> 'Beberapa',
                'baumann_answer' => 'Beberapa',
                'value' => 3
            ],
            [
                'id_question'=> '14',
                'baumann_no'=> '24',
                'quiz_answer'=> 'Banyak',
                'baumann_answer' => 'Banyak',
                'value' => 4
            ],
            [
                'id_question'=> '14',
                'baumann_no'=> '24',
                'quiz_answer'=> 'Pertanyaan ini tidak berlaku untuk saya (karena saya laki-laki, atau karena saya tidak pernah hamil atau minum pil KB atau HRT, atau karena saya tidak yakin apakah saya memiliki flek hitam)',
                'baumann_answer' => 'Pertanyaan ini tidak berlaku untuk saya (karena saya laki-laki, atau karena saya tidak pernah hamil atau minum pil KB atau HRT, atau karena saya tidak yakin apakah saya memiliki flek hitam)',
                'value' => 2.5
            ],

            [
                'id_question'=> '15',
                'baumann_no'=> '25',
                'quiz_answer'=> 'Saya tidak memiliki flek hitam',
                'baumann_answer' => 'Saya tidak memiliki flek hitam',
                'value' => 1
            ],
            [
                'id_question'=> '15',
                'baumann_no'=> '25',
                'quiz_answer'=> 'Tidak yakin',
                'baumann_answer' => 'Tidak yakin',
                'value' => 2
            ],
            [
                'id_question'=> '15',
                'baumann_no'=> '25',
                'quiz_answer'=> 'Sedikit lebih buruk',
                'baumann_answer' => 'Sedikit lebih buruk',
                'value' => 3
            ],
            [
                'id_question'=> '15',
                'baumann_no'=> '25',
                'quiz_answer'=> 'Jauh lebih buruk',
                'baumann_answer' => 'Jauh lebih buruk',
                'value' => 4
            ],

            [
                'id_question'=> '0',
                'baumann_no'=> '27',
                'quiz_answer'=> 1,
                'baumann_answer' => 'Tidak',
                'value' => null
            ],
            [
                'id_question'=> '0',
                'baumann_no'=> '28',
                'quiz_answer'=> 1,
                'baumann_answer' => 'Tidak',
                'value' => null
            ],

            [
                'id_question'=> '0',
                'baumann_no'=> '27',
                'quiz_answer'=> 2,
                'baumann_answer' => 'Saya tidak yakin',
                'value' => null
            ],
            [
                'id_question'=> '0',
                'baumann_no'=> '28',
                'quiz_answer'=> 2,
                'baumann_answer' => 'Ya, beberapa (satu sampai lima)',
                'value' => null
            ],

            [
                'id_question'=> '0',
                'baumann_no'=> '27',
                'quiz_answer'=> 3,
                'baumann_answer' => 'Ya, sedikit terlihat',
                'value' => null
            ],
            [
                'id_question'=> '0',
                'baumann_no'=> '28',
                'quiz_answer'=> 3,
                'baumann_answer' => 'Ya, banyak (enam sampai lima belas)',
                'value' => null
            ],

            [
                'id_question'=> '0',
                'baumann_no'=> '27',
                'quiz_answer'=> 4,
                'baumann_answer' => 'Ya, sangat mencolok',
                'value' => null
            ],
            [
                'id_question'=> '0',
                'baumann_no'=> '28',
                'quiz_answer'=> 4,
                'baumann_answer' => 'Ya, sangat banyak (enam belas atau lebih)',
                'value' => null
            ],

            [
                'id_question'=> '15',
                'baumann_no'=> '26',
                'quiz_answer'=> 'Saya tidak memiliki flek hitam',
                'baumann_answer' => 'Tidak',
                'value' => null
            ],
            [
                'id_question'=> '15',
                'baumann_no'=> '26',
                'quiz_answer'=> 'Tidak yakin',
                'baumann_answer' => 'Tidak',
                'value' => null
            ],
            [
                'id_question'=> '15',
                'baumann_no'=> '26',
                'quiz_answer'=> 'Sedikit lebih buruk',
                'baumann_answer' => 'Ya',
                'value' => null
            ],
            [
                'id_question'=> '15',
                'baumann_no'=> '26',
                'quiz_answer'=> 'Jauh lebih buruk',
                'baumann_answer' => 'Ya',
                'value' => null
            ],

            [
                'id_question'=> '20',
                'baumann_no'=> '32',
                'quiz_answer'=> 'Sekali sebulan',
                'baumann_answer' => 'Satu sampai lima tahun',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '32',
                'quiz_answer'=> 'Sering',
                'baumann_answer' => 'Lima sampai sepuluh tahun',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '32',
                'quiz_answer'=> 'Setiap hari',
                'baumann_answer' => 'Lebih dari sepuluh tahun',
                'value' => null
            ],

            [
                'id_question'=> '20',
                'baumann_no'=> '33',
                'quiz_answer'=> 'Sekali sebulan',
                'baumann_answer' => 'Satu sampai lima tahun',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '33',
                'quiz_answer'=> 'Setiap hari',
                'baumann_answer' => 'Lima sampai sepuluh tahun',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '33',
                'quiz_answer'=> 'Setiap hari',
                'baumann_answer' => 'Lebih dari sepuluh tahun',
                'value' => null
            ],

            [
                'id_question'=> '20',
                'baumann_no'=> '35',
                'quiz_answer'=> 'Sering',
                'baumann_answer' => 'Sekali seminggu',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '35',
                'quiz_answer'=> 'Setiap hari',
                'baumann_answer' => 'Sehari-hari',
                'value' => null
            ],

            [
                'id_question'=> '20',
                'baumann_no'=> '36',
                'quiz_answer'=> 'Sekali sebulan',
                'baumann_answer' => 'Satu sampai lima kali',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '36',
                'quiz_answer'=> 'Sering',
                'baumann_answer' => 'Lima sampai sepuluh kali',
                'value' => null
            ],
            [
                'id_question'=> '20',
                'baumann_no'=> '36',
                'quiz_answer'=> 'Setiap hari',
                'baumann_answer' => 'Berkali-kali',
                'value' => null
            ],

            [
                'id_question'=> '23',
                'baumann_no'=> '40',
                'quiz_answer'=> 'Tidak pernah',
                'baumann_answer' => '0–10 persen',
                'value' => null
            ],
            [
                'id_question'=> '23',
                'baumann_no'=> '40',
                'quiz_answer'=> 'Kadang-kadang',
                'baumann_answer' => '10–25 persen',
                'value' => null
            ],
            [
                'id_question'=> '23',
                'baumann_no'=> '40',
                'quiz_answer'=> 'Sekali sehari',
                'baumann_answer' => '25–75 persen',
                'value' => null
            ],
            [
                'id_question'=> '23',
                'baumann_no'=> '40',
                'quiz_answer'=> 'Di setiap makan',
                'baumann_answer' => '75–100 persen',
                'value' => null

            ],
        ]);
    }
}

