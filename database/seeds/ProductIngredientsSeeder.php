<?php

use App\ProductIngredient;
use Illuminate\Database\Seeder;

class ProductIngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductIngredient::truncate();

        $csvFile = fopen(public_path("seeds/product_ingredient_seed.csv"), "r");

        $firstLine = true;
        while(($data = fgetcsv($csvFile, 2000, ",")) !== false) {
            
            if(!$firstLine){
                ProductIngredient::create([
                    "product_name" => $data['0'],
                    "ingredient" => $data['1']
                ]);
            }

            $firstLine = false;

        }

        fclose($csvFile);
        
    }
}
