<?php

use App\SkinTypeIngredient;
use Illuminate\Database\Seeder;

class SkinTypeIngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SkinTypeIngredient::truncate();

        $csvFile = fopen(public_path("seeds/skin_type_ingredient_seed.csv"), "r");

        $firstLine = true;
        while(($data = fgetcsv($csvFile, 2000, ";")) !== false) {
            
            if(!$firstLine){
                SkinTypeIngredient::create([
                    "skin_type_id" => $data['0'],
                    "ingredient" => $data['1']
                ]);
            }

            $firstLine = false;

        }

        fclose($csvFile);
        
    }
}
