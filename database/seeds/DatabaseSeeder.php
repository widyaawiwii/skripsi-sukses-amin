<?php

use App\ProductIngredient;
use App\SkinTypeDetail;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(MappingBaumannTableSeeder::class);
        $this->call(SkinTypeDetailsSeeder::class);
        $this->call(SkinTypeIngredientsSeeder::class);
        $this->call(ProductDetailsSeeder::class);
        $this->call(ProductIngredientsSeeder::class);
    }
}
