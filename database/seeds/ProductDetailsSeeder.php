<?php

use App\ProductDetail;
use Illuminate\Database\Seeder;
use Illuminate\support\facades\DB;
use Goutte\Client;

class ProductDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductDetail::truncate();
        
        // scraping products details from https://www.beautyhaul.com/product/skincare/all
        $httpClient = new Client();

        $url = "https://www.beautyhaul.com/product/skincare/all";
        $webpageidx = 1;
        $headers = @get_headers($url);
        while($headers && strpos($headers[0], "200") && $webpageidx < 73){

            $response = $httpClient->request('GET', $url);
            // get prices into an array
            $productBrands = [];
            $response->filter('.row.products.mb-0 div a div.caption div.brand')->each(function ($node) use (&$productBrands) {
                $productBrands[] = $node->text();
            });

            $productNames = [];
            $response->filter('.row.products.mb-0 div a div.caption div.title')->each(function ($node) use (&$productNames) {
                $productNames[] = $node->text();
            });

            $productImages = [];
            $response->filter('.col-d2.col-md-d3.col-lg-d4.product-item a div picture img')->each(function ($node) use (&$productImages) {
                if($node->nodeName() == 'img'){
                    $productImages[] = $node->attr('src');
                }
            });

            $brandsIndex = 0;
            $imagesIndex = 0;
            $values = "";
            foreach ($productNames as $name){
                $brand = $productBrands[$brandsIndex];
                $image = $productImages[$imagesIndex];

                ProductDetail::create([
                    "product_name" => $name,
                    "product_brand" => $brand,
                    "product_image" => $image
                ]);

                $brandsIndex++;
                $imagesIndex++;
            }
            
            $webpageidx++;
            $url = "https://www.beautyhaul.com/product/skincare/all/" . $webpageidx;
            $headers = @get_headers($url);
        }

    }
}
