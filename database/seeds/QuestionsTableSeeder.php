<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            [
                'questions'=> 'Setelah mencuci muka, jangan gunakan pelembab, tabir surya, toner, bedak, atau produk lainnya. Dua sampai tiga jam kemudian, lihat cermin di bawah cahaya terang. Dahi dan pipi Anda terasa atau terlihat:',
                'choices'=> 'Sangat kasar, berkelupas, atau pucat;Kencang;Terhidrasi dengan baik dan tidak terlihat berminyak;Terhidrasi dengan baik dan terlihat berminyak',
                'type'=> 'radio',
                'baumann_no' => '1',
                'image' => ''
            ],
            [
                'questions'=> 'Wajah Anda terasa berminyak',
                'choices'=> 'Tidak pernah;Kadang-kadang;Sering;Selalu',
                'type'=> 'radio',
                'baumann_no' => '2',
                'image' => ''
            ],
            [
                'questions'=> 'Dua sampai tiga jam setelah memakai make up foundation, kulit Anda terasa:',
                'choices'=> 'Flaky, atau cakey;Halus;Terlihat berminyak;Terlihat bergaris-garis dan berminyak;Tidak menggunakan make up',
                'type'=> 'radio',
                'baumann_no' => '3',
                'image' => 'assets/img/cakey.png'
            ],
            [
                'questions'=> 'Tanpa memakai moisturizer atau sunscreen, kulit Anda terasa:',
                'choices'=> 'Kering dan pecah-pecah;Kencang;Normal;Terlihat berminyak, atau tidak pernah merasa membutuhkan moisturizer;Tidak tahu karena selalu memakai moisturizer/sunscreen',
                'type'=> 'radio',
                'baumann_no' => '4;7;8',
                'image' => ''
            ],
            [
                'questions'=> 'Menurut Anda, jenis kulit Anda :',
                'choices'=> 'Kering;Normal;Kombinasi;Berminyak',
                'type'=> 'radio',
                'baumann_no' => '5',
                'image' => ''
            ],
            [
                'questions'=> 'Ketika menggunakan sabun yang bergelembung atau berbusa banyak, kulit wajah Anda terasa:',
                'choices'=> 'Terasa kering atau pecah-pecah;Terasa sedikit kering tapi tidak pecah-pecah;Terasa biasa saja;Terasa berminyak;Saya tidak menggunakan sabun atau pembersih berbusa lainnya. (Jika ini karena sabun berbusa membuat kulit Anda kering, pilih pilihan pertama)',
                'type'=> 'radio',
                'baumann_no' => '6',
                'image' => ''
            ],
            [
                'questions'=> 'Apakah Anda pernah didiagnosis dengan jerawat atau rosacea?',
                'choices'=> 'Tidak;Ya',
                'type'=> 'radio',
                'baumann_no' => '11',
                'image' => 'assets/img/rosacea.jpg'
            ],
            [
                'questions'=> 'Pilih kondisi terkini yang sesuai dengan Anda:',
                'choices'=> 'Terkadang atau sering ada jerawat merah;Produk skincare / makeup bisa menyebabkan jerawatan, kemerahan, gatal, kepedihan, atau bengkak;Perhiasan yang bukan emas bisa menyebabkan kemerahan;Sunscreen bisa menyebabkan kemerahan atau gatal;Cincin bisa menyebabkan kemerahan;Bubble bath / massage oil / body lotion bisa menyebabkan gatal atau kering;Sabun dari hotel bisa menyebabkan gatal, kemerahan, atau jerawatan;Tidak ada yang sesuai',
                'type'=> 'checkbox',
                'baumann_no' => '9;10;12;13;14;15;16;21',
                'image' => ''
            ],
            [
                'questions'=> 'Pilih hal-hal yang menyebabkan wajah Anda menjadi merah:',
                'choices'=> 'Setelah olahraga / saat stres / beremosi kuat (seperti marah);Setelah makan pedas atau mengkonsumsi panas;Tidak ada yang sesuai',
                'type'=> 'checkbox',
                'baumann_no' => '17;18',
                'image' => ''
            ],
            [
                'questions'=> 'Berapa banyak pembuluh darah merah atau biru yang terlihat (atau pernah Anda miliki sebelum perawatan) di wajah dan hidung Anda?',
                'choices'=> 'Tidak ada;Sedikit (satu sampai tiga di seluruh wajah, termasuk hidung);Beberapa (empat hingga enam di seluruh wajah, termasuk hidung);Banyak (lebih dari tujuh di seluruh wajah, termasuk hidung)',
                'type'=> 'radio',
                'baumann_no' => '19',
                'image' => 'assets/img/pembuluh darah.png'
            ],
            [
                'questions'=> 'Orang-orang pernah bertanya apabila kulit Anda terbakar sinar matahari, padahal sebenarnya tidak sedang terbakar:',
                'choices'=> 'Tidak pernah;Kadang-kadang;Sering;Selalu;Saya selalu terbakar matahari',
                'type'=> 'radio',
                'baumann_no' => '20',
                'image' => ''
            ],
            [
                'questions'=> 'Setelah Anda memiliki jerawat atau ingrown hair (rambut yang tumbuh ke dalam, bukan keluar kulit), akan diikuti oleh bintik gelap atau hitam kecoklatan: (area kulit mencakup seluruh bagian tubuh termasuk : di atas bibir, pipi, dada, punggung, lengan)',
                'choices'=> 'Tidak pernah atau saya tidak memperhatikan;Kadang-kadang;Sering;Selalu',
                'type'=> 'radio',
                'baumann_no' => '22',
                'image' => ''
            ],
            [
                'questions'=> 'Setelah terluka, berapa lama bekas luka (setelah berwarna cokelat, bukan merah) tetap ada? (area kulit mencakup seluruh bagian tubuh termasuk : di atas bibir, pipi, dada, punggung, lengan):',
                'choices'=> 'Saya tidak mendapatkan bekas luka atau saya tidak memperhatikan;Seminggu;Beberapa minggu;Berbulan-bulan',
                'type'=> 'radio',
                'baumann_no' => '23',
                'image' => ''
            ],
            [
                'questions'=> 'Berapa banyak bintik atau flek hitam yang muncul di wajah Anda saat hamil, mengonsumsi pil KB, atau menjalani terapi penggantian hormon (HRT)? (area kulit mencakup seluruh bagian tubuh termasuk : di atas bibir, pipi, dada, punggung, lengan)',
                'choices'=> 'Tidak ada;Satu;Beberapa;Banyak;Pertanyaan ini tidak berlaku untuk saya (karena saya laki-laki, atau karena saya tidak pernah hamil atau minum pil KB atau HRT, atau karena saya tidak yakin apakah saya memiliki flek hitam)',
                'type'=> 'radio',
                'baumann_no' => '24',
                'image' => 'assets/img/flek.png'
            ],
            [
                'questions'=> 'Apakah flek hitam di wajah Anda bertambah parah saat Anda berjemur di bawah sinar matahari (area kulit mencakup seluruh bagian tubuh termasuk : di atas bibir, pipi, dada, punggung, lengan) ?',
                'choices'=> 'Saya tidak memiliki flek hitam;Tidak yakin;Sedikit lebih buruk;Jauh lebih buruk',
                'type'=> 'radio',
                'baumann_no' => '25;26',
                'image' => 'assets/img/flek.png'
            ],
            [
                'questions'=> 'Apakah Anda memiliki kerutan wajah?',
                'choices'=> 'Tidak;Hanya ketika saya menggerakkan wajah, seperti tersenyum, mengerutkan kening, atau mengangkat alis;Ya, sedikit saat menggerakkan wajah dan sedikit saat diam tanpa gerakan;Kerutan muncul bahkan jika saya tidak tersenyum, mengerutkan kening, atau mengangkat alis',
                'type'=> 'radio',
                'baumann_no' => '29',
                'image' => ''
            ],
            [
                'questions'=> 'Menurut Anda, terlihat seperti umur berapa wajah ibu Anda?',
                'choices'=> 'Lima hingga sepuluh tahun lebih muda dari usianya;Sesuai usianya;Lima tahun lebih tua dari usianya;Lebih dari lima tahun lebih tua dari usianya;Tak dapat diterapkan. Saya diadopsi atau saya tidak ingat',
                'type'=> 'radio',
                'baumann_no' => '30',
                'image' => ''
            ],
            [
                'questions'=> 'Menurut Anda, terlihat seperti umur berapa wajah ayah Anda?',
                'choices'=> 'Lima hingga sepuluh tahun lebih muda dari usianya;Sesuai usianya;Lima tahun lebih tua dari usianya;Lebih dari lima tahun lebih tua dari usianya;Tak dapat diterapkan. Saya diadopsi atau saya tidak ingat',
                'type'=> 'radio',
                'baumann_no' => '31',
                'image' => ''
            ],
            [
                'questions'=> 'Menurut Anda, Anda terlihat seperti usia berapa?',
                'choices'=> 'Satu sampai lima tahun lebih muda dari usia saya;Sesuai usia;Lima tahun lebih tua dari usia saya;Lebih dari lima tahun lebih tua dari usia saya',
                'type'=> 'radio',
                'baumann_no' => '34',
                'image' => ''
            ],
            [
                'questions'=> 'Seberapa sering Anda terekspos sinar matahari ataupun menjalani prosedur tanning dalam 5 tahun terakhir?',
                'choices'=> 'Tidak pernah;Sekali sebulan;Sering;Setiap hari',
                'type'=> 'radio',
                'baumann_no' => '32;33;35;36',
                'image' => ''
            ],
            [
                'questions'=> 'Tolong jelaskan polusi udara di tempat Anda tinggal:',
                'choices'=> 'Udaranya segar dan bersih;Udara sangat tercemar',
                'type'=> 'radio',
                'baumann_no' => '37',
                'image' => ''
            ],
            [
                'questions'=> 'Jelaskan berapa lama Anda menggunakan krim wajah retinoid seperti retinol, Renova, Retin-A, Tazorac, Diacerin, atau Avage:',
                'choices'=> 'Bertahun-tahun;Kadang-kadang;Sekali untuk jerawat ketika saya masih muda;Tidak pernah',
                'type'=> 'radio',
                'baumann_no' => '38',
                'image' => ''
            ],
            [
                'questions'=> 'Seberapa sering Anda mengonsumsi buah dan sayur',
                'choices'=> 'Tidak pernah;Kadang-kadang;Sekali sehari;Di setiap makan',
                'type'=> 'radio',
                'baumann_no' => '39;40',
                'image' => ''
            ],
            [
                'questions'=> 'Apa warna kulit alami Anda (tanpa tanning atau selftanner)?',
                'choices'=> 'Gelap;Sedang;Putih;Sangat putih',
                'type'=> 'radio',
                'baumann_no' => '41',
                'image' => 'assets/img/skintone.png'
            ],
            ]);
    }
}
