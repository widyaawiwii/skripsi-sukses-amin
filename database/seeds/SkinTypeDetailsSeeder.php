<?php

use App\SkinTypeDetail;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class SkinTypeDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        SkinTypeDetail::truncate();

        $csvFile = fopen(public_path("seeds/skin_type_detail_seed.csv"), "r");

        $firstLine = true;
        while(($data = fgetcsv($csvFile, 2000, ";")) !== false) {
            
            if(!$firstLine){
                SkinTypeDetail::create([
                    "skin_type_id" => $data['0'],
                    "skin_type_name" => $data['1'],
                    "skin_type_desc_p1" => $data['2'],
                    "skin_type_desc_p2" => $data['3'],
                    "skin_type_desc_p3" => $data['4'],
                    "skin_type_desc_img" => $data['5'],
                    "skin_type_face_img" => $data['6']
                ]);
            }

            $firstLine = false;

        }

        fclose($csvFile);
        
    }
}
