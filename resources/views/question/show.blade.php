<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>How to Get Previous and Next Record in Laravel - ItSolutionstuff.com</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container mt-5">
        <h1>How to Get Previous and Next Record in Laravel - ItSolutionstuff.com</h1>

        <h2>{{ $post->title }}</h2>
        <p>{{ $post->body }}</p>

        <div class="row">
            <div class="col-6">
                @if (isset($post->previous))
                    {{-- <a href="{{ route('question.show', $post->previous->slug) }}"> --}}
                        <div> Previous</div>
                        <p>{{ $post->previous->title }}</p>
                    </a>
                @endif
            </div>

            <div class="col-6">
                @if (isset($question->next))
                    {{-- <a href="{{ route('question.show', $post->next->slug) }}"> --}}
                        <div>Next</div>
                        <p>{{ $question->next->id }}</p>
                    </a>
                @endif
            </div>
        </div>

    </div>
</body>

</html>
