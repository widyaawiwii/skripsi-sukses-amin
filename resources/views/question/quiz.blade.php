@extends('master')

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div align="right">
                {{-- <a href="{{url('question/question1')}}" class="btn btn-primary">Quiz</a> --}}
                <br />
                <br />
            </div>
            <div class="content">
                <div class="title m-b-md">
                    {{-- {{$question->questions}} --}}
                    {{-- Homepage --}}
                </div>
                {{-- <form method="post" class="box bos-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label><input type="checkbox" name="isian[]" value="Jeruk">Jeruk</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
                </form> --}}
                {{-- <table class="table table-bordered table-striped"> --}}
{{--
                     {{$questions->questions}} <br>
                        @foreach($choicesArr as $row)

                            <label><input type="checkbox" name="questions" class="form-control" value="$row">{{"$row"}}</label><br>
                        @endforeach --}}


                    <form method="post" action="{{url('question')}}">
                        {{csrf_field()}}
                        {{$questions->questions}}<br>
                        @foreach($choicesArr as $row)
                        {{-- {{'questions'.$row}} --}}
                        {{'questions'.$loop->index}}
                        {{$row}}
                            <input type="checkbox" name={{'questions'.$loop->index}} id="checkBox" class="form-control" value=0 <?php echo (isset($_POST['checkbox']))? "checked='checked'": "";?>>{{"$row"}}<br>
                        @endforeach
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" />
                            {{-- <input type="submit" class="btn btn-primary" href="{{url('question/'.$next.'/quiz')}}">next<br> --}}
                        </div>
                        {{-- <a href="{{url('question/'.$next.'/quiz')}}" class="btn btn-primary" onclick="passAnswer();">next</a> --}}
                    </form>

                    <a href="{{url('question/'.$next.'/quiz')}}" class="btn btn-primary" onclick="passAnswer();">next</a>
                    {{-- <a class="btn btn-primary" onclick="showAnswer();">show</a> --}}
                    <a href="{{url('question/'.$prev.'/quiz')}}" class="btn btn-primary" onclick="passAnswer();">prev</a>

            </div>
        </div>
    </body>
</html>


<script>
    function passAnswer() {
        let questions1, questions2;
        questions1 = document.getElementById("checkBox").value;
        questions2 = document.getElementById("checkBox").value;
        // var firstname=document.getElementById("txt").value;
        // localStorage.setItem("textvalue",firstname);
        localStorage.setItem("questions1",questions1);
        localStorage.setItem("questions2",questions2);
        // return false;
    }
    function saveToDatabase() {

    }
</script>
