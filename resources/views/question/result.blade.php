<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Apa jenis kulit Anda? Berdasarkan Baumann Skin Type Solutions</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <style>
        .divquestion {
            width: 80%;
            margin-left: 10%;
            margin-right: 10%;
        }

        a:hover {
            color: rgb(65, 120, 88);
            background-color: transparent;
        }

    </style>
    </style>
</head>

<body>

  
  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="50000">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          
          <div class="carousel-item active">
            <div class="carousel-background"><img src="assets/img/slide/slide-1.jpg" alt=""></div>
            
              <div class="carousel-container">
                <div style="display:inline-block;z-index:1;position:fixed;right:0px;top:0px"><img src="assets/img/cta/cta-shape.svg" style="max-width: 200px" alt="service" /></div>
            
                  <div class="carousel-content">
                      <h6>Jenis Kulit Wajah Anda:</h6>
                      
                      <div style="background-image:url(assets/img/hero/shape.svg);background-position:55% 80%;background-repeat:no-repeat">
                        <h1 style="margin:20px"class="animate__animated animate__fadeInDown" >{{$skinType}}</h1>
                          
                      </div>
                      <h5 style="margin-bottom:20px;color:#e83e8c"class="animate__animated animate__fadeInDown" >{{ $skin_type_name[0].' '.$oiliness.'% • '.$skin_type_name[1].' '.$sensitivity.'% • '.$skin_type_name[2].' '.$pigmentation.'% • '.$skin_type_name[3].' '.$pigmentation.'%' }}</h5>
                    
                    
                    <div class="animate__animated animate__fadeInUp" style="padding-top:35px;padding-bottom:10px;background-color: rgba(223, 215, 249, 0.199);border-radius: 129px 20px 20px 20px;position:relative;margin-left:70px;margin-right:70px">
                      
                       
                        <div class=""> <img src="assets/img/cta/shape-bg2.png" style= "display:inline-block;z-index:1;position:absolute;right:0;top:0" width="264" alt="cta shape" /></div>
                        <div class=""> <img src="assets/img/cta/shape-bg1.png" style="display:block;z-index:1;position:absolute;start:0px;bottom:0px;max-width: 340px;" alt="cta shape" /></div>
                        <p style="color:#3d4f4c"> {{ $skin_type_desc_p1 }} </p>
                        <p style="color:#3d4f4c"> {{ $skin_type_desc_p2 }} </p>
                        <p style="color:#3d4f4c"> {{ $skin_type_desc_p3 }} </p>
                        
                    </div>
                  </div>
            </div>
          </div>

      
          <div class="carousel-item">
            <div class="carousel-background"><img src="assets/img/slide/slide-2.jpg" alt=""></div>
            <div class="carousel-container"  style="background-image:url(assets/img/hero/hero-bg.svg);">
           
              <div class="carousel-content">
                <h1 class="animate__animated animate__fadeInDown">Bahan Perawatan yang Sesuai untuk Anda</h1>
                <div class="" style="padding-top:35px;padding-bottom:10px;border-radius: 129px 20px 20px 20px;position:relative;margin-left:70px;margin-right:70px">
                    <!--loop ingredients-->
                    @foreach($skin_type_ingrs as $ingr)
                      <a style="color:#334240;display:inline-block;margin:5px" class="btn-start">{{ $ingr }}</a>
                    @endforeach
                  </div>
                
              </div>
              
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="assets/img/slide/slide-3.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h1 style="margin-top:30px"class="animate__animated animate__fadeInDown">Rekomendasi Produk untuk Anda</h1>
                <!-- {{-- <h3 class="animate__animated animate__fadeInDown">{{ $products->product_name }}</h3><br>
                <h2 class="animate__animated animate__fadeInUp">{{ $products->product_brand }}</h2><br> --}}
                {{-- <img src="{{ $products->product_image }}"> --}} -->
                <section id="services" class="services">
                  <div class="container">
                    <div class="row">
                      <!--loop product recommendation-->
                        @foreach($products as $product)
                        <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                          <div class="icon-box">
                            @if(strlen($product->product_name) > 30)
                          <h4 class="title"><a href="">{{ substr($product->product_name,0,30).'...' }}</a></h4>
                            @else
                          <h4 class="title"><a href="">{{ $product->product_name }}</a></h4>
                            @endif
                          <p class="description">by {{ $product->product_brand }}</p>
                            <div>
                              <img style="object-fit:cover"src={{ $product->product_image }} class="img-fluid" alt="" width="300" height="700">
                            </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </section>
                {{-- <a style = "position:fixed;right:120px;top:90px"href="https://www.beautyhaul.com/product/skincare/all/" target="_blank"class="animate__animated animate__fadeInDown">See More</a> --}}

                
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
          <span style="color:#1bbca3" class="carousel-control-prev-icon bi bi-chevron-double-left" aria-hidden="true"></span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
          <span style="color:#1bbca3" class="carousel-control-next-icon bi bi-chevron-double-right" aria-hidden="true"></span>
        </a>

      </div>
    </div>
  </section>
  <!-- End Hero -->


    <main id="main">

        <!-- Vendor JS Files -->
        <script src="assets/vendor/purecounter/purecounter.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
        <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>

</body>

</html>
