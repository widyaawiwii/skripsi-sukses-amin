@extends('master')

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div align="right">
                {{-- <a href="{{url('question/question1')}}" class="btn btn-primary">Quiz</a> --}}
                <br />
                <br />
            </div>
            <div class="content">
                    <form method="post" action="{{url('question')}}">
                        {{csrf_field()}}
                        {{$questions->questions}}<br>
                        @foreach($choicesArr as $row)
                        {{-- {{'questions'.$row}} --}}
                        {{-- {{'questions'.$loop->index}} --}}
                        <h1>{{$row}}
                        {{-- <br>print: {{$loop->index}} --}}
                            {{-- <input type="checkbox" name=questions1 id={{$questions class="form-control" value={{$loop->index}}>{{"$row"}}<br> --}}
                            <input type="checkbox" name=questions1 >
                        @endforeach
                        {{-- <div class="form-group"> --}}
                            <input type="submit" class="btn btn-primary" />
                        {{-- </div> --}}
                    </form>
{{--
                    <a href="{{url('question1/2')}}" class="btn btn-primary">next</a>
                    <a href="{{url('question1/1')}}" class="btn btn-primary">prev</a> --}}
            </div>
        </div>
    </body>
</html>


{{-- <script>
    function passAnswer() {
        let questions1, questions2;
        questions1 = document.getElementById("checkBox").value;
        questions2 = document.getElementById("checkBox").value;
        // var firstname=document.getElementById("txt").value;
        // localStorage.setItem("textvalue",firstname);
        localStorage.setItem("questions1",questions1);
        localStorage.setItem("questions2",questions2);
        // return false;
    }
    function saveToDatabase() {

    }

var answer = document.querySelectorAll("input[type='checkbox']");

for (var i = 0; i < answer.length; i++) {
    var ans = answer[i];
    if (ans.hasAttribute("store")) {
        setupBox(box);
    }
}

function setupBox(box) {
    var storageId = box.getAttribute("store");
    var oldVal    = localStorage.getItem(storageId);
    box.checked = oldVal === "true" ? true : false;

    box.addEventListener("change", function() {
        localStorage.setItem(storageId, this.checked);
    });
}
</script> --}}
