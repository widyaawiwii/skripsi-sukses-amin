<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Apa jenis kulit Anda? Berdasarkan Baumann Skin Type Solutions</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <style>
        .divquestion {
            width: 55%;
            /* margin-left: 1%; */
        }
    </style>
</head>

<body>

<section class="info-box py-0">
    <div class="container-fluid bg-holder"  style="background-image:url(assets/img/hero/hero-bg.svg);">
        <div style="display:inline-block;z-index:1;position:fixed;right:0px;top:0px"><img src="assets/img/cta/cta-shape.svg" style="max-width: 200px" alt="service" /></div>
            
        <form method="get" class="divquestion" action="http://localhost:8000/result">
            @foreach ($questionArr as $row)
                @if ($row->id != 1)
                    @if ($row->type == 'checkbox')
                    <div id="{{ $row->id }}" class="row" style="display:none">
                        <div class=" d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                        
                            <div class="content">
                                
                                <h3 class="animate__animated animate__fadeInDown"><strong>{{$row->id}}/24</strong></h3>
                                <p class="animate__animated animate__fadeInDown">
                                {{ $row->questions }}
                                </p>
                            </div>
                            
                            <div class="accordion-list">
                                <ul>
                                @foreach (explode(';', $row->choices) as $rowChoice)
                                <li>
                                    <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                                    <input type="hidden" name="{{ $row->id }}[]" value="NOOOOO">
                                    <label>
                                        <input type="{{ $row->type }}" name="{{ $row->id }}[]" value="{{ $rowChoice }}" >&ensp;&nbsp;{{ $rowChoice }}
                                    </label>
                                    </div>
                                </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="position:fixed;top:150px;right:20px">
                            <img src="{{ $row->image }}" class="img-fluid animate__animated animate__fadeInDown" style="height:250px" alt="">
                        </div>
                    </div>
                    @else
                    <div id="{{ $row->id }}" class="row" style="display:none">
                        <div class=" d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                        
                            <div class="content">
                                
                                <h3 class="animate__animated animate__fadeInDown"><strong>{{$row->id}}/24</strong></h3>
                                <p class="animate__animated animate__fadeInDown">
                                {{ $row->questions }}
                                </p>
                            </div>
                            
                            <div class="accordion-list">
                                <ul>
                                @foreach (explode(';', $row->choices) as $rowChoice)
                                <li>
                                    <div id="accordion-list-2" class="collapse show" data-bs-parent=".accordion-list">
                                    <label>
                                        <input type="{{ $row->type }}" id="{{ 'radio'.$row->id }}"  name="{{ $row->id }}[]" value="{{ $rowChoice }}" >&ensp;&nbsp;{{ $rowChoice }}
                                    </label>
                                    </div>
                                </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="position:fixed;top:150px;right:20px">
                            <img src="{{ $row->image }}" class="img-fluid animate__animated animate__fadeInDown" style="height:250px" alt="">
                        </div>
                    </div>
                    @endif
                @else
                    @if ($row->type == 'checkbox')
                    <div id="{{ $row->id }}" class="row">
                        <div class=" d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                        
                            <div class="content">
                                
                                <h3 class="animate__animated animate__fadeInDown"><strong>{{$row->id}}/24</strong></h3>
                                <p class="animate__animated animate__fadeInDown">
                                {{ $row->questions }}
                                </p>
                            </div>
                            
                            <div class="accordion-list">
                                <ul>
                                @foreach (explode(';', $row->choices) as $rowChoice)
                                <li>
                                    <div id="accordion-list-3" class="collapse show" data-bs-parent=".accordion-list">
                                    <input type="hidden" name="{{ $row->id }}[]" value="NOOOOO">
                                    <label>
                                        <input type="{{ $row->type }}" name="{{ $row->id }}[]" value="{{ $rowChoice }}" >&ensp;&nbsp;{{ $rowChoice }}
                                    </label>
                                    </div>
                                </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="position:fixed;top:150px;right:20px">
                            <img src="{{ $row->image }}" class="img-fluid animate__animated animate__fadeInDown" style="height:250px" alt="">
                        </div>
                    </div>
                    @else
                    <div id="{{ $row->id }}" class="row">
                        <div class=" d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                        
                            <div class="content">
                                
                                <h3 class="animate__animated animate__fadeInDown"><strong>{{$row->id}}/24</strong></h3>
                                <p class="animate__animated animate__fadeInDown">
                                {{ $row->questions }}
                                </p>
                            </div>
                            
                            <div class="accordion-list">
                                <ul>
                                @foreach (explode(';', $row->choices) as $rowChoice)
                                <li>
                                    <div id="accordion-list-4" class="collapse show" data-bs-parent=".accordion-list">
                                    <label>
                                        <input type="{{ $row->type }}" name="{{ $row->id }}[]" value="{{ $rowChoice }}" >&ensp;&nbsp;{{ $rowChoice }}
                                    </label>
                                    </div>
                                </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="position:fixed;top:150px;right:20px">
                            <img src="{{ $row->image }}" class="img-fluid animate__animated animate__fadeInDown" style="height:250px" alt="">
                        </div>
                    </div>
                    @endif
                @endif
            
            @endforeach
            <div style="position:fixed;bottom:20px;left:112px" class="animate__animated animate__fadeInUp">
                <a class="spc-button animate__animated scrollto" style="display: none" id="prevbtn">Kembali</a>
                <a class="spc-button animate__animated scrollto" style="display: inline-block" id="nextbtn">Selanjutnya</a>
                <input id="submitbtn" type="submit" class="spc-button animate__animated scrollto" style="display: none;" />
             
            </div>        
            
            
        </form>
        
      </div>
    </section>



    <!-- ======= Hero Section ======= -->
    <!-- <section id="hero">
        <div class="hero-container">
            <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

                   
                    <div class="carousel-item active">
                        <div class="bg-holder" style="background-image:url(assets/img/hero/hero-bg.svg);"></div>
                        <div class="carousel-container">

                            <form method="get" class="divquestion" action="http://127.0.0.1:8000/result">
                                @foreach ($questionArr as $row)
                                    @if ($row->id != 1)
                                        @if ($row->type == 'checkbox')
                                            <div id={{ $row->id }} style="display: none">
                                                <p class="animate__animated animate__fadeInDown" style="color:#5E6282">
                                                    {{ $row->questions }}</p><br>
                                                @foreach (explode(';', $row->choices) as $rowChoice)
                                                    <input type="hidden" name="{{ $row->id }}[]" value="NOOOOO">
                                                    <p style="color:#5E6282"><input type="{{ $row->type }}"
                                                            name="{{ $row->id }}[]" value="{{ $rowChoice }}"
                                                            style="color: white">&ensp;&nbsp;{{ $rowChoice }}</p><br>
                                                @endforeach
                                            </div>
                                        @else
                                            <div id={{ $row->id }} style="display: none;">
                                                <p style="color:#5E6282" class="animate__animated animate__fadeInDown">
                                                    {{ $row->questions }}</p><br>
                                                @foreach (explode(';', $row->choices) as $rowChoice)
                                                    <p style="color:#5E6282" style="color:white"><input type="{{ $row->type }}"
                                                            name="{{ $row->id }}[]" value="{{ $rowChoice }}"
                                                            style="color: white">&ensp;&nbsp;{{ $rowChoice }}</p><br>
                                                @endforeach
                                            </div>
                                        @endif
                                    @else
                                        @if ($row->type == 'checkbox')
                                            <div class="" id={{ $row->id }}>
                                                <p style="color:#5E6282" class="animate__animated animate__fadeInDown">
                                                    {{ $row->questions }}</p><br>
                                                @foreach (explode(';', $row->choices) as $rowChoice)
                                                    <input type="hidden" name="{{ $row->id }}[]" value="NOOOOO">
                                                    <p style="color:#5E6282"><input class="checkbox" type="{{ $row->type }}"
                                                            name="{{ $row->id }}[]" value="{{ $rowChoice }}"
                                                            style="color: white">&ensp;&nbsp;{{ $rowChoice }}</p><br>
                                                @endforeach
                                            </div>
                                        @else
                                            <div class="" id={{ $row->id }}>
                                                <p style="color:#5E6282" class="animate__animated animate__fadeInDown">
                                                    {{ $row->questions }}</p><br>
                                                @foreach (explode(';', $row->choices) as $rowChoice)
                                                    <p style="color:#5E6282"><input type="{{ $row->type }}"
                                                            name="{{ $row->id }}[]" value="{{ $rowChoice }}"
                                                            style="color: white">&ensp;&nbsp;{{ $rowChoice }}</p><br>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif
                                @endforeach

                                <div>
                                    <a class="btn-get-started animate__animated scrollto" style="display: none" id="prevbtn" style="display: none;">prev</a>
                                    <a class="btn-get-started animate__animated scrollto" style="display: inline-block" id="nextbtn">next</a>
                                </div>
                                <input id="submitbtn" type="submit" class="btn-get-started animate__animated scrollto" style="display: none;" />

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            </div>
    </section> -->



    <main id="main">



        <!-- Vendor JS Files -->
        <script src="assets/vendor/purecounter/purecounter.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
        <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>

        <script>

        </script>

        <script>

            var i = 1;
            var max = {{ $maxArr }};
            var type = "a";

            document.getElementById('nextbtn').onclick = function(e) {
                console.log('halo');
                var namadiv = i;
                var namadivafter = parseInt(i) + 1;
                i++;

                if (i == 1) {
                    document.getElementById('nextbtn').style.display = "inline-block";
                    document.getElementById('prevbtn').style.display = "none";
                    document.getElementById('submitbtn').style.display = "none";
                } else if (i == max) {
                    document.getElementById('nextbtn').style.display = "none";
                    document.getElementById('prevbtn').style.display = "inline-block";
                    document.getElementById('submitbtn').style.display = "inline-block";
                } else {
                    document.getElementById('nextbtn').style.display = "inline-block";
                    document.getElementById('prevbtn').style.display = "inline-block";
                    document.getElementById('submitbtn').style.display = "none";
                }

                document.getElementById(namadivafter).style.display = "inline-block";
                document.getElementById(namadiv).style.display = "none";

            }

            document.getElementById('prevbtn').onclick = function(e) {
                var namadiv = i;
                var namadivbefore = parseInt(i) - 1;
                i--;

                if (i == 1) {
                    document.getElementById('nextbtn').style.display = "inline-block";
                    document.getElementById('prevbtn').style.display = "none";
                    document.getElementById('submitbtn').style.display = "none";
                } else if (i == max) {
                    document.getElementById('nextbtn').style.display = "none";
                    document.getElementById('prevbtn').style.display = "inline-block";
                    document.getElementById('submitbtn').style.display = "inline-block";
                } else {
                    document.getElementById('nextbtn').style.display = "inline-block";
                    document.getElementById('prevbtn').style.display = "inline-block";
                    document.getElementById('submitbtn').style.display = "none";
                }

                document.getElementById(namadivbefore).style.display = "inline-block";
                document.getElementById(namadiv).style.display = "none";

            }

            // document.getElementById(i).onclick = function(e) {
            //     var checkBox = document.getElementById(i);
            //     var text = document.getElementById("text");

            //     if (checkBox.checked == true){
            //         text.style.display = "block";
            //     } else {
            //         text.style.display = "none";
            //     }
            // }

            // function myFunction() {
            //     var checkBox = document.getElementById(i);
            //     var text = document.getElementById("text");
            //         if (checkBox.checked == true){
            //             text.style.display = "block";
            //         } else {
            //             text.style.display = "none";
            //         }
            //     }
            // }
        </script>
    </main>
</body>

</html>
