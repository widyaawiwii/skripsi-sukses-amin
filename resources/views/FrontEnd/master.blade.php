<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Apa jenis kulit Anda? Berdasarkan Baumann Skin Type Solutions</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Shuffle - v4.7.0
  * Template URL: https://bootstrapmade.com/bootstrap-3-one-page-template-free-shuffle/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<section class="info-box py-0">
      <div class="container-fluid bg-holder"  style="background-image:url(assets/img/hero/hero-bg.svg);">
      
      <!-- <img class="img" src="assets/img/hero/hero-img.png" alt="hero-header" /> -->

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1" style="position:fixed;top:100px">

            <div class="content animate__animated animate__fadeInDown" >
              <h3>Selamat datang di <strong>Skin Type Quiz</strong>!</h3>
              <p>
                Ketahui komposisi bahan dan produk yang sesuai untuk jenis kulit Anda menurut <strong>Baumann Skin Type Solutions</strong>!
                <br>
                <p style="font-size: 10px">
                Telah digunakan di dalam berbagai buku medis dan oleh ratusan ahli dermatologi, 
                <br>Baumann Skin Type Solutions mengkategorikan karakteristik kulit wajah menjadi 16 tipe yang telah terbukti secara ilmiah.
                <br>Pengkategorian didasari oleh 4 indikator kesehatan kulit yaitu <strong><i>Oiliness</i></strong> (tingkat produksi minyak), <strong><i>Sensitivity</i></strong> (tingkat sensitivitas), <strong><i>Pigmentation</i></strong> (tingkat pigmentasi), dan <strong><i>Tightness</i></strong> (tingkat kekencangan kulit).
                <br>Dengan mengikuti kuis ini, Anda dapat mempelajari bahan dan produk skincare yang cocok untuk rutinitas perawatan Anda dan meningkatkan kesehatan kulit wajah Anda.
                </p>
              </p>
            </div>
           
            <div style="margin-top:25px"class="accordion-list animate__animated animate__fadeInUp scrollto">
            <a href="{{url('questions')}}" class="btn-start ">Mulai Kuis</a>
            </div>

          </div>
          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="position:fixed;bottom:0;right:50px">
              <img src="assets/img/face.png" height=400 width=400 class="img-fluid animate__animated animate__fadeInUp" alt="">
          </div>
        </div>

      </div>
    </section>

  <!-- ======= Hero Section ======= -->
  <!-- <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox"> -->

          <!-- Slide 1 -->
          <!-- <div class="carousel-item active">
            <div class="carousel-background"><img src="assets/img/slide/slide-1.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Welcome to <span>SkinType</span></h2>
                <p class="animate__animated animate__fadeInUp">Get your personlized skincare ingredients and products based on your skin type!</p>
                <a href="{{url('questions')}}" class="btn-get-started animate__animated animate__fadeInUp scrollto">Start Quiz</a>
              </div>
            </div>
          </div> -->

          <!-- Slide 2-->
          <!-- <div class="carousel-item">
            <div class="carousel-background"><img src="assets/img/slide/slide-2.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="{{url('questions')}}" class="btn-get-started animate__animated animate__fadeInUp scrollto">Quiz</a>
              </div>
            </div>
          </div> -->

          <!-- Slide 3 -->
          <!-- <div class="carousel-item">
            <div class="carousel-background"><img src="assets/img/slide/slide-3.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Sequi ea ut et est quaerat</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="{{url('questions')}}" class="btn-get-started animate__animated animate__fadeInUp scrollto">Quiz</a>
              </div>
            </div>
          </div> -->

        <!-- </div> -->
<!-- 
        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon bi bi-chevron-double-left" aria-hidden="true"></span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon bi bi-chevron-double-right" aria-hidden="true"></span>
        </a> -->

      <!-- </div>
    </div>
  </section>End Hero -->



  <main id="main">



  <!-- Vendor JS Files -->
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
