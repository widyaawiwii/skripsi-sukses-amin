<?php

use App\Http\Controllers\QuestionController;
use App\Question;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('student', 'StudentController');

// welcome page
Route::get('/', 'TemplateController@index');

// questions page
Route::get('questions','QuestionController@getAllQuestion');
Route::post('questions','QuestionController@store');

// result page
Route::get('result','UserAnswerController@hitApi');
