<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkinTypeDetail extends Model
{
    protected $fillable = ['skin_type_id', 'skin_type_name', 'skin_type_desc_p1', 'skin_type_desc_p2', 'skin_type_desc_p3', 'skin_type_desc_img', 'skin_type_face_img'];

    public $timestamps = false;
}
