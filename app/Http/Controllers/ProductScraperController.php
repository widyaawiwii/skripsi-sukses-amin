<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\support\facades\DB;
use Goutte\Client;

class ProductScraperController extends Controller
{
    public function scrape_product_detail(){

        // scraping products details from https://www.beautyhaul.com/product/skincare/all
        // require '../vendor/autoload.php';
        $httpClient = new Client();

        $url = "https://www.beautyhaul.com/product/skincare/all";
        $webpageidx = 1;
        $headers = @get_headers($url);
        // while($headers && strpos($headers[0], "200") && $webpageidx < 3){
        while($headers && strpos($headers[0], "200") && $webpageidx < 73){

            echo "page " . $webpageidx . "<br>";
            echo $url . "<br>";
            $response = $httpClient->request('GET', $url);
            // get prices into an array
            $productBrands = [];
            $response->filter('.row.products.mb-0 div a div.caption div.brand')->each(function ($node) use (&$productBrands) {
                $productBrands[] = $node->text();
                // echo $node->text();
            });

            $productNames = [];
            $response->filter('.row.products.mb-0 div a div.caption div.title')->each(function ($node) use (&$productNames) {
                $productNames[] = $node->text();
            });

            $productImages = [];
            $response->filter('.col-d2.col-md-d3.col-lg-d4.product-item a div picture img')->each(function ($node) use (&$productImages) {
                if($node->nodeName() == 'img'){
                    $productImages[] = $node->attr('src');
                }
            });

            $brandsIndex = 0;
            $imagesIndex = 0;
            $values = "";
            foreach ($productNames as $name){
                $brand = $productBrands[$brandsIndex];
                $image = $productImages[$imagesIndex];
                
                $name = str_replace("'", "\'", trim($name, " "));
                $brand = str_replace("'", "\'", $brand);
                echo "PRODUCT NAME : {$name}<br>BRAND : {$brand}<br>IMAGE URL : {$image}<br><br>";
                $values .= "('{$name}', '{$brand}', '{$image}'),";
                // DB::insert('insert into product_details (product_name, product_brand, product_image) values (?, ?, ?)', [$name, $brand, $image]);
                // DB::insert('insert into product_detail (product_name, product_brand, product_image) values (?, ?, ?)', [$name, $brand, " "]);
    
                $brandsIndex++;
                $imagesIndex++;
            }

            // bulk insert to db
            $query = "insert into product_details (product_name, product_brand, product_image) values ";
            if(strlen($values) > 0){
                $values = substr($values, 0, strlen($values)-1); // exclude trailing ,

                echo $query . $values;
                DB::insert($query . $values);
            }

            $webpageidx++;
            $url = "https://www.beautyhaul.com/product/skincare/all/" . $webpageidx;
            $headers = @get_headers($url);
        }

        
        // echo sizeof($productNames) . " " . sizeof($productBrands) . " " . sizeof($productImages);

 
        // $isNull = DB::select('select count(*) as TOTAL from product_details');
        // $isNull = $isNull[0]->TOTAL;

        // if ($isNull > 0) {
        //     DB::delete('delete from product_details');
        // }

        // $products = DB::table('product_details')->first();

        // return view('FrontEnd.home');

        // return view('question.result', compact('products'));

        // echo sizeof($productNames) . " " . sizeof($productBrands) . " " . sizeof($productImages);

    }

    public function scrape_product_ingredient(){

        // scraping products and ingredients from https://www.beautyhaul.com/product/skincare/all

        // cleanup db
        $isNull = DB::select('select count(*) as TOTAL from product_details');
        $isNull = $isNull[0]->TOTAL;

        if ($isNull > 0) {
            DB::delete('delete from product_details');
        }

        // nanat ngide barbar way to scrape multiple pages - ga baik buat aplikasi yg bagus wkwk
        $httpClient = new Client();

        $url = "https://www.beautyhaul.com/product/skincare/all";
        $webpageidx = 1;
        $headers = @get_headers($url);
       
        while($headers && strpos($headers[0], "200") && $webpageidx < 73){
            echo "<br> page " . $webpageidx . "<br>";
            $response = $httpClient->request('GET', $url);

            $productNames = [];
            $response->filter('.row.products.mb-0 div a div.caption div.title')->each(function ($node) use (&$productNames) {
                $productNames[] = $node->text();
            });
     
            // echo name and ingredients
            $nameIndex = 0;
            $response->filter('.row.products.mb-0 div a')->each(function ($node) use ($productNames, &$nameIndex, $httpClient) {
                $name = $productNames[$nameIndex];
                $url = $node->link()->getUri();
                $crawler = $httpClient->request('GET', $url);
                $ingredients = [];
                try{
                    $crawler->filter('#overview > div > div > div:nth-child(3) > div.text.col-sm-13.offset-sm-4 > div > div')->each(function ($node) use (&$ingredients){
                        $ingredients[] = $node->text();        
                    });
                    
                    $unset = "";
                    foreach($ingredients as $ing){
                        if(strpos($ing, ",")){
                            $unset = $ing;
                            $token = strtok($ing, ",");
                            
                            while($token !== false && strpos($ing, ",") !== "1"){
                                array_push($ingredients, $token);
                                $token = strtok(",");
                            }
                        }
                        
                    }
    
                    $ingidx = 0;
                    $values = "";
                    echo "<br><br>NAME : {$name}<br>INGREDIENTS :<br>";
                    $name = str_replace("'", "\'", trim($name, " "));
                    foreach($ingredients as $ing){
                        if($ing == $unset){
                            unset($ingredients[$ingidx]);
                        }
                        else{
                            $ing = str_replace("'", "\'", trim($ing, " "));
                            echo "    - {$ing}<BR>";
                            $values .= "('{$name}', '{$ing}'),";
                            // DB::insert('insert into product_ingredient (product_name, ingredient_name) values (?, ?)', [$name, $ing]);
                        }
                        $ingidx++;
                    }

                    // bulk insert to db
                    $query = "insert into product_ingredient (product_name, ingredient_name) values ";
                    if(strlen($values) > 0){
                        $values = substr($values, 0, strlen($values)-1); // exclude trailing ,

                        echo $query . $values;
                        DB::insert($query . $values);
                    }

                    echo "<br><br>";

                }catch(\InvalidArgumentException $e){
     
                    echo "\n\nNO ING FOUND FOR {$name}\n\n";
                }
     
                $nameIndex++;
     
            });
     
            $webpageidx++;
            $url = "https://www.beautyhaul.com/product/skincare/all/" . $webpageidx;
            $headers = @get_headers($url);
        }
   }
}
