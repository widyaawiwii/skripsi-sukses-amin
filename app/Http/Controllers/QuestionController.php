<?php

namespace App\Http\Controllers;

use App\Question;
use App\UserAnswer;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class QuestionController extends Controller
{

    public function getQuestion()
    {
        // $user = User::find(1);
        $question = Question::find(1);
        return view('question.index', ['question' => $question]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all()->toArray();
        return view('question.index', compact('questions'));
        // $question = Question::find(1);
        // return view('question.index',['question'=>$question]);
    }

    public function getAllQuestion()
    {
        $questionArr = Question::all();
        $maxArr = count($questionArr);
        // echo shell_exec("python C:/Users/ACER/Desktop/All/Skripsi/pakeComposer/test.py");
        // dd($questionArr);
        return view('question.question', compact('questionArr', 'maxArr'));
    }

    public function getChoices($id)
    {
        $questions = Question::find($id);
        // print_r($id);
        $choices = Question::find($id)->choices;
        $choicesArr = explode(';', $choices);

        // return view('question.edit', compact('question', 'id'));

        // $questions = Question::all()->toArray();
        // print_r($choicesArr);
        // print_r($choices);

        // $question = Question::find($id);
        // return view('question.index', compact('questions','id'));

        // return view('question.quiz', compact('questions','choicesArr','prev','next'));
        return view('question.question1', compact('questions', 'choicesArr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function question1()
    {
        return view('question.question1');
    }


    public function create()
    {
        // return view('question.question1');
    }

    public function result($id) {

        $result = UserAnswer::find($id)->result;
        $test = "test";
        $questionArr = Question::all();
        $maxArr = count($questionArr);
        return view('question.result', compact('result','test','maxArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'first_name'    =>  'required',
        //     'last_name'     =>  'required'
        // ]);
        // $question = new Question([
        //     'questions'    =>  $request->get('questions'),
        //     'choices'    =>  $request->get('choices')
        // ]);
        // $question->save();
        // return redirect()->route('question.index')->with('success', 'Data Added');

        $questions1 = $request->get('1');
        $questions2 = $request->get('2');
        $questions3 = $request->get('3');
        // $questions4 = $request->get('4');
        // $questions5 = $request->get('5');
        // $questions6 = $request->get('1')[2];

        $answer = $questions1.';'.$questions2.';'.$questions3.';';
        $process = new Process(['py', "C:\Users\ACER\Desktop\All\Skripsi\pakeComposer\\test.py", "{$answer}"]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $data = $process->getOutput();
        dd($data);
        $userAnswer = new UserAnswer([
            // 'questions1'    =>  $request->get('1'), // nama field => id di indexnya


            'questions1'    =>  $questions1,
            'questions2'    =>  $questions2,
            'questions3'    =>  $questions3,
            'result' => $data
            // ,
            // 'questions4'    =>  $questions4,
            // 'questions5'    =>  $questions5,
            // 'questions6'    =>  $questions6

            // 'questions1'    => $request->localStorage
        ]);
        $userAnswer->save();

        // dd($request->get('1')[1]);
        // return redirect()->route('question.index')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        return view('question.edit', compact('question', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'questions'    =>  'required',
            'choices'     =>  'required'
        ]);
        $question = Question::find($id);
        $question->questions = $request->get('questions');
        $question->choices = $request->get('choices');
        $question->save();
        return redirect()->route('student.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $student = Question::find($id);
        // $student->delete();
        // return redirect()->route('student.index')->with('success', 'Data Deleted');
    }
}
