<?php

namespace App\Http\Controllers;
use App\Question;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Illuminate\Http\Request;
use Illuminate\support\facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\UserAnswer;
use Facade\Ignition\SolutionProviders\MissingImportSolutionProvider;

// use App\Http\Controllers\Process;


class UserAnswerController extends Controller
{
    /**
     * @Route("/", name="home", methods={"POST"})
     */
    public function hitApi(Request $request)
    {
        $questions_no = 0;
        // baumann questions
        $total_questions = 41;
        $questionArr = Question::all();
        $cntVal8 = 0;
        $cntVal9 = 0;
        $cntVal10 = 0;

        while($questions_no < $total_questions) {

            // id question
            $questions_no = $questions_no+1;

            if($questions_no != 27 && $questions_no != 28) {
                // get questions id
                $query = 'select id from questions where baumann_no LIKE "%'.$questions_no.'%" order by id limit 1';
                $q_questions_id = DB::select($query);
                $questions_id = $q_questions_id[0]->id;

                // get all baumann no for $questions_no
                $q_questionsDB = DB::select('select baumann_no,type from questions where id = ?',[$questions_id]);
                $baumannNoDB = $q_questionsDB[0]->baumann_no;
                // put baumann no to array
                $baumannNoArr = explode(";", $baumannNoDB);

                // get type of questions (radio/checkbox)
                $type = $q_questionsDB[0]->type;
                // get input from user
                $input = $request->get($questions_id);

                // if checkbox then must read from array
                if ($type == 'radio') $arr_no = 0;
                else {

                    // for make variable cntVal[number] that use checkbox type
                    $cnt = 'cntVal'.$questions_id;

                    // logic to check whether checkbox is checked or no
                    $size = sizeof($input);
                    $value = $size - count(array_keys($input, "NOOOOO"));
                    $arr_no = $$cnt;
                    if ($$cnt+1 < $size) {
                        if ($input[$$cnt+1] == "NOOOOO") {
                            $arr_no = $$cnt;
                            $$cnt = $$cnt+1;
                        } else {
                            $arr_no = $arr_no+1;
                            $$cnt = $arr_no+1;
                        }
                    } else {
                        $arr_no = $$cnt;
                    }

                }

                // special case for last question
                if ($questions_no == $total_questions) $arr_no = 0;

                // get answer for save to DB
                $questions = 'questions'.$questions_no;
                $$questions = $input[$arr_no];

            }

        }

        // save original answer to user)answer table
        $userAnswer = new UserAnswer([
            'questions1' => $questions1, 'questions2' => $questions2,
            'questions3' => $questions3, 'questions4' => $questions4,
            'questions5' => $questions5, 'questions6' => $questions6,
            'questions7' => $questions7, 'questions8' => $questions8,
            'questions9' => $questions9, 'questions10' => $questions10,
            'questions11' => $questions11, 'questions12' => $questions12,
            'questions13' => $questions13, 'questions14' => $questions14,
            'questions15' => $questions15, 'questions16' => $questions16,
            'questions17' => $questions17, 'questions18' => $questions18,
            'questions19' => $questions19, 'questions20' => $questions20,
            'questions21' => $questions21,
            'questions22' => $questions22,
            'questions23' => $questions23,
            'questions24' => $questions24,
            'questions25' => $questions25,
            'questions26' => $questions26,
            'questions29' => $questions29, 'questions30' => $questions30,
            'questions31' => $questions31, 'questions32' => $questions32,
            'questions33' => $questions33, 'questions34' => $questions34,
            'questions35' => $questions35, 'questions36' => $questions36,
            'questions37' => $questions37, 'questions38' => $questions38,
            'questions39' => $questions39, 'questions40' => $questions40,
            'questions41' => $questions41, 'result'    => 'aaaaaaa'
        ]);
        $userAnswer->save();

        // copy answer from user_answers to baumann_user_answers
        DB::insert('INSERT into baumann_user_answers SELECT * FROM user_answers where status = 0');
        $questions_no = 0;

        // start mapping
        while($questions_no < $total_questions) {
            $questions_no = $questions_no+1;
            $questions = 'questions'.$questions_no;
            $value = 'value'.$questions_no;

            // query for get baumann_answer from mapping_baumann table
            $q_mapping = '
                SELECT baumann_answer, value
                FROM mapping_baumann m, baumann_user_answers b
                WHERE baumann_no = '.$questions_no.
                ' AND status = 0
                AND quiz_answer = (
                    SELECT '.$questions.'
                    FROM baumann_user_answers
                    WHERE  status = 0
                )';

            $mapping = DB::select($q_mapping);
            // print('q no:'.$questions_no);
            // print_r($mapping);

            // update answer based on mapping_baumann
            if ($mapping != null) {
                $q_update = 'update baumann_user_answers set '.$questions.' = '."'".$mapping[0]->baumann_answer."'".
                            'where status = 0';
                DB::update($q_update);
            }

            // update value for PvsN questions (id 22-25)
            if($questions_no >= 22 & $questions_no <= 25) {
                $q_value = 'update baumann_user_answers set '.$value.' = '."'".$mapping[0]->value."'".
                            'where status = 0';
                DB::update($q_value);

                // if last question of PvsN question
                if($questions_no == 25) {
                    // get total value from question 22-25
                    $q_pvsn = '
                        select value22+value23+value24+value25 as "value"
                        from baumann_user_answers where status = 0';
                    $pvsn = DB::select($q_pvsn);
                    $val_pvsn = $pvsn[0]->value;

                    // mapping to score 1-4
                    if ($val_pvsn >= 4 && $val_pvsn <= 7) $val_pvsn = 1;
                    else if ($val_pvsn >= 8 && $val_pvsn <= 11) $val_pvsn = 2;
                    else if ($val_pvsn >= 12 && $val_pvsn <= 15) $val_pvsn = 3;
                    else if ($val_pvsn == 16) $val_pvsn = 4;

                    // update pvsn answer based on mapping table
                    // q27 (pvsn no 4)
                    $q_update_pvsn = '
                        update baumann_user_answers
                        set questions27 = (
                        select baumann_answer
                        from mapping_baumann
                        where baumann_no = 27 and quiz_answer ='.$val_pvsn.
                        ') where status = 0';
                    DB::update($q_update_pvsn);

                    // q28 (pvsn no 7)
                    $q_update_pvsn = '
                    update baumann_user_answers
                    set questions28 = (
                    select baumann_answer
                    from mapping_baumann
                    where baumann_no = 28 and quiz_answer ='.$val_pvsn.
                    ') where status = 0';
                    DB::update($q_update_pvsn);
                }
            }

        }

        // update status supaya gak dicek lagi, status = 1 artinya udah dimapping
        DB::update('update baumann_user_answers set status = 1');
        DB::update('update user_answers set status = 1');

        $total_questions = 41;
        $questions_no = 0;

        // get all answer from db
        while($questions_no < $total_questions) {

            // get max id in db
            $q_id = 'select max(id) as "id" from baumann_user_answers';
            $id = DB::select($q_id);
            $id = $id[0]->id;

            //
            $questions_no = $questions_no+1;
            $questions = 'questions'.$questions_no;
            // $$questions = $input[$arr_no];

            $q_questions = 'select '.$questions.' from baumann_user_answers where id = '.$id;
            $$questions = DB::select($q_questions);
            $$questions = $$questions[0]->$questions;

        }

        $postdata = json_encode(
            array(
                //1-8
                'oiliness' => array($questions1,$questions2,$questions3,$questions4,$questions5,$questions6,$questions7,$questions8),
                //9-21
                'sensitivity' => array($questions9,$questions10,$questions11,$questions12,$questions13,$questions14,$questions15,$questions16,$questions17,$questions18,$questions19,$questions20,$questions21),
                //22-28
                'pigmentation' => array($questions22,$questions23,$questions24,$questions25,$questions26,$questions27,$questions28),
                //29-41
                'tightness' => array($questions29,$questions30,$questions31,$questions32,$questions33,$questions34,$questions35,$questions36,$questions37,$questions38,$questions39,$questions40,$questions41)
            )
        );

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);
        $url = 'http://127.0.0.1:5000/skin-type';
        $result = file_get_contents($url, false, $context);
        $result = json_decode($result, true);
        $skinType = $result['skin-type'];
        $oiliness = $result['oiliness'];
        $tightness = $result['tightness'];
        $sensitivity = $result['sensitivity'];
        $pigmentation = $result['pigmentation'];

        // fetch skin type details from DB
        $select_skin_type = (array) DB::table('skin_type_details')
                                      ->where('skin_type_id', '=', $skinType)
                                      ->first();               
        $skin_type_name = explode(' ', $select_skin_type['skin_type_name']);
        $skin_type_desc_p1 = $select_skin_type['skin_type_desc_p1'];
        $skin_type_desc_p2 = $select_skin_type['skin_type_desc_p2'];
        $skin_type_desc_p3 = $select_skin_type['skin_type_desc_p3'];

        // fetch suitable ingredients
        $skin_type_ingrs = DB::table('skin_type_ingredients')
                                ->where('skin_type_id', '=', $skinType)
                                ->distinct()
                                ->pluck('ingredient');

        // fetch product recommendations based on suitable ingredients
        $ingredients_string = implode(",", $skin_type_ingrs->toArray());
        $products = DB::table('product_details')
                      ->select('product_details.product_name', 'product_details.product_brand', 'product_details.product_image')
                      ->join('product_ingredients', 'product_details.product_name', '=', 'product_ingredients.product_name')
                      ->wherein('product_ingredients.ingredient', $skin_type_ingrs->toArray())
                      ->inRandomOrder()
                      ->take(4)
                      ->get();

                    //   dd($products);
        return view('question.result', compact('skinType', 'products', 'skin_type_name', 'skin_type_desc_p1', 'skin_type_desc_p2', 'skin_type_desc_p3', 'skin_type_ingrs', 'oiliness', 'sensitivity', 'tightness', 'pigmentation'));

    }

    public function index(Request $request) {

        // $answer = $request->get('2').$request->get('2');
        // $process = new Process(['py', "C:\Users\ACER\Desktop\All\Skripsi\pakeComposer\\test.py", "{$answer}"]);

        // $process->run();

        // if (!$process->isSuccessful()) {
        //     throw new ProcessFailedException($process);
        // }

        $questions1 = $request->get('1')[2];
        $questions2 = $request->get('2');
        $questions3 = $request->get('3')[2];

        $answer = $questions1.';'.$questions2.';'.$questions3.';';
        $process = new Process(['py', "C:\Users\ACER\Desktop\All\Skripsi\pakeComposer\\test.py", "{$answer}"]);
        $process->run();


        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $data = $process->getOutput();
        // print("data:");
        // print($data);
        // dd($data);

        $data = 'Dry';

        // print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        $skinType1 = Question::where('skin', $data)->pluck('id');
        $choices = Question::find(1)->choices;

        // print($data);
        print($skinType1);
        // print($choices);
        // dd($skinType);

        // dd($answer);
        return view('question.result', compact('answer', 'data'));
    }



}
