<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkinTypeIngredient extends Model
{
    protected $fillable = ['skin_type_id', 'ingredient'];

    public $timestamps = false;
}
