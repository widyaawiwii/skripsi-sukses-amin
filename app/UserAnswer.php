<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $fillable = [
        'questions1', 'questions2', 'questions3', 'questions4', 'questions5',
        'questions6','questions7' ,'questions8' ,'questions9' ,'questions10',
        'questions11','questions12','questions13','questions14','questions15',
        'questions16','questions17','questions18','questions19','questions20',
        'questions21','questions22','questions23','questions24','questions25',
        'questions26','questions27','questions28','questions29','questions30',
        'questions31','questions32','questions33','questions34','questions35',
        'questions36','questions37','questions38','questions39','questions40',
        'questions41','result'
    //, 'questions4', 'questions5', 'questions6'
    ];
}
