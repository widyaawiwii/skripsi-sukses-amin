<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $fillable = ['product_name', 'product_brand', 'product_image'];
    
    public $timestamps = false;
}
